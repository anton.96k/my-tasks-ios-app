//
//  TasksViewPresenter.swift
//  My Tasks
//
//  Created by User on 11/30/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation
import UIKit

protocol TasksView {
    func displayUserTasks(_ category: TaskStatus)
    func clearList()
}

class TasksViewPresenter {
    
    var tasksView: TasksView?
    let localDB: DBHelper
    
    init() {
        localDB = DBHelper()
    }
    
    /*!
     * This method is used to assign a view to this presenter
     */
    func attachView(_ view: TasksView) {
        self.tasksView = view
    }
    
    /*!
     * This method is used to unassign a view from this presenter
     */
    func detachView() {
        self.tasksView = nil
    }
    
    /*!
     * This method is used to obtain a list of tasks from the database
     */
    func getUserTasks(_ category: TaskStatus)  -> [UserTask] {
        
        var userTasksList: [UserTask] = []
        
        userTasksList = localDB.getUserTasks(category)
        
        return userTasksList
    }
    
    /*!
     * @brief This method is used for deleting the tasks from the databse
     */
    func deleteUserTask(_ userTaskID: String){
        localDB.deleteUserTask(userTaskID)
    }
    
}
