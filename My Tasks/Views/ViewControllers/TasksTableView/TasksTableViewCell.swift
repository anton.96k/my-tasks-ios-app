//
//  TasksTableViewCell.swift
//  My Tasks
//
//  Created by User on 11/30/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import UIKit

class TasksTableViewCell: UITableViewCell {

    @IBOutlet weak var taskStatusImageView: UIImageView!
    @IBOutlet weak var taskTitleLabel: UILabel!
    
    var userTaskID: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
