//
//  FirstViewController.swift
//  My Tasks
//
//  Created by User on 11/30/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import UIKit
import UserNotifications

class TasksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UNUserNotificationCenterDelegate, TasksView {
    
    // MARK: - UI Outlets and local varibales
   
    // UI Outlets
    @IBOutlet weak var taskCategorySelectorSegmentedControl: UISegmentedControl!
    @IBOutlet weak var taskListBackgroundView: UIView!
    @IBOutlet weak var tasksTableView: TasksTableView!
    
    // Local dependencies
    fileprivate var presenter: TasksViewPresenter?
    fileprivate var userTasksList: [UserTask] = []
    
    // Local varibales that are needed to store temporary info, flags etc
    fileprivate var selectedUserTaskID = ""
    fileprivate var currentUserTaskCategory = TaskStatus.IN_PROGRESS
    
    // MARK: - ViewController lifecycle methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Obtaining an instance of the presenter
        presenter = TasksViewPresenter()
        presenter?.attachView(self)
        
        // Making this view controller in charge of the tableview
        tasksTableView.delegate = self
        tasksTableView.dataSource = self
        
        // Adding rounder corners to the tableview background
        taskListBackgroundView.layer.cornerRadius = 8
        
        // Requesting the notifications permissions
        UNUserNotificationCenter.current().requestAuthorization(
                            options: [[.alert, .sound, .badge]],
                            completionHandler: {
                                (granted, error) in
                            }
        )
        UNUserNotificationCenter.current().delegate = self
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        // Clearing the selections inside the tablewview in case we return here from another screen
        if let indexPath = tasksTableView.indexPathForSelectedRow {
            self.tasksTableView.deselectRow(at: indexPath, animated: true)
        }
        
        // Showing the list of tasks from the database
        displayUserTasks(currentUserTaskCategory)
        
    }
    
    // MARK: - UI methods and actions
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    /*!
     * This method defines the number if sections in our tableview
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /*!
     * This method defines the nuber of rows in the tableview
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userTasksList.count
    }
    
    /*!
     * This method sets up each cell
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Obtaining the cell identifier
        let cellIdentifier = StoryboardIdentifiers.TASKS_TABLEVIEW_CELL
        
        // Finding the cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TasksTableViewCell else {
                fatalError("The dequeued cell is not an instance of \(cellIdentifier)")
        }
        
        // Setting up the cell with the data
        let userTask = userTasksList[indexPath.row]
        cell.userTaskID = userTask.id
        cell.taskTitleLabel.text = userTask.title
        
        // Setting up the icon for the task according to it's status
        if (userTask.isDone) {
            cell.taskStatusImageView.image = UIImage(named: AssetsConstants.GOAL_ICON)
        } else {
            cell.taskStatusImageView.image = UIImage(named: AssetsConstants.WALK_ICON)
        }
        
        return cell
    }
    
    /*!
     * This method defines the logic for editin and deleting rows
     */
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            // Telling the presenter to delete this task from the database
            presenter?.deleteUserTask(userTasksList[indexPath.row].id)
            
            // Removing the row from the table and it's item from the list of tasks
            userTasksList.remove(at: indexPath.row)
            tasksTableView.deleteRows(at: [indexPath], with: .fade)
            
        } else if editingStyle == .insert {
            
        }
        
    }
    
    /*!
     * This method is used to register a click on the cell
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        // Getting the current cell from the index path
        let currentCell = tableView.cellForRow(at: indexPath)! as! TasksTableViewCell
        selectedUserTaskID = currentCell.userTaskID
        
        // Performing a segue to the task detasils view controller
        performSegue(withIdentifier: StoryboardIdentifiers.SHOW_TASK_DETAILS_SEGUE, sender: self)
        
    }
    
    /*!
     * This method is called right before the segue will be executed
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        // If we are going to transition using the "show task detauls" segue
        // we will pass the task ID to the destination view controller
        if segue.identifier == StoryboardIdentifiers.SHOW_TASK_DETAILS_SEGUE {
            let destinationViewController = segue.destination as? TaskDetailsViewController
            destinationViewController?.userTaskID = selectedUserTaskID
        }
        
    }
    
    /*!
     * This method is used to change the category of tasks that will be displayed
     */
    @IBAction func taskCategorySelectionChanged(_ sender: Any) {
        
        switch taskCategorySelectorSegmentedControl.selectedSegmentIndex {
            case 0:
                currentUserTaskCategory = TaskStatus.IN_PROGRESS
                displayUserTasks(currentUserTaskCategory)
            case 1:
                currentUserTaskCategory = TaskStatus.DONE
                displayUserTasks(currentUserTaskCategory)
            default:
                currentUserTaskCategory = TaskStatus.IN_PROGRESS
                displayUserTasks(currentUserTaskCategory)
            }
        
    }
    
    // MARK: - Custom methods

    /*!
     * This method is used to display user tasks
     */
    func displayUserTasks(_ category: TaskStatus) {
        
        // Reloading the tableview with the data
        userTasksList = (presenter?.getUserTasks(category))!
        
        // Sorting the data to make the recently created tasks appear on top of the list
        userTasksList = userTasksList.sorted{ $0.notificationDate > $1.notificationDate }
        
        // Refreshing the tableview
        tasksTableView.reloadData()
        
    }
    
    /*!
     * This method cleans up the tableview
     */
    func clearList() {
        userTasksList = []
        tasksTableView.reloadData()
    }
    
}

