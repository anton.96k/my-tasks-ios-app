//
//  AddNewTaskViewController.swift
//  My Tasks
//
//  Created by User on 11/30/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import UIKit
import UserNotifications

class TaskEditorViewController: UIViewController, UITextViewDelegate, TaskEditorView {
    
    // MARK: - UI Outlets and local varibales
    
    // UI Outlets
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var reminderSwitch: UISwitch!
    
    
    // Needed for inner views
    fileprivate var descriptionTextViewCanBeCleared: Bool!
    var isNewTask = true
    fileprivate var dateAsString: String!
    fileprivate var date: Date!
    
    // Dependencies
    fileprivate var presenter: TaskEditorPresenter!
    
    // Local variables
    var userTaskID: String!
    fileprivate var userTask: UserTask!
    
    // MARK: - ViewContorller lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        presenter = TaskEditorPresenter()
        presenter.attachView(self)
        
        // Stylyzing the title text view
        titleTextField.layer.borderColor = UIColor.lightGray.cgColor
        titleTextField.layer.borderWidth = 0.3
        titleTextField.layer.cornerRadius = 8
        titleTextField.textColor = UIColor.lightGray
        
        // Setting this view controller as a delegate for the descriptionTextView
        descriptionTextView.delegate = self

        // Stylyzing the description text view
        descriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTextView.layer.borderWidth = 0.3
        descriptionTextView.layer.cornerRadius = 8
        descriptionTextView.text = "enter description"
        
        // Checking if we are createing a new task or editing the existing one
        if isNewTask {
            
            // Setting up the input fields to be clear
            descriptionTextViewCanBeCleared = true
            navBarTitle.title = "New Task"
            titleTextField.text = ""
            descriptionTextView.text = "enter description"
            
        } else {
            
            // Getting an info abot the task from the databse
            userTask = presenter.getTaskByID(userTaskID)
            
            // Setting up the input fields
            descriptionTextViewCanBeCleared = false
            navBarTitle.title = "Edit Task"
            titleTextField.text = userTask.title
            descriptionTextView.text = userTask.description
            
        }
        
    }
    
    // MARK: - UI methods and actions

    /*!
     * Changes the appearance of the text when user types something in the textview
     */
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if descriptionTextViewCanBeCleared {
            descriptionTextView.text = ""
        }
        
    }
    
    /*!
     * Changes the appearance of the text when user stops editing the text inside the textview
     */
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if descriptionTextView.text.isEmpty != true {
            descriptionTextViewCanBeCleared = false
        } else {
            descriptionTextView.text = "enter description"
            descriptionTextViewCanBeCleared = true
        }
        
    }
    
    @IBAction func datePickerValueChanged(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        dateAsString = dateFormatter.string(from: datePicker.date)
        date = datePicker.date
        
    }
    
    /*!
     * Called when user wants to save the task
     */
    @IBAction func save(_ sender: UIBarButtonItem) {
        
        if isNewTask{
            presenter.addNewTask(titleTextField.text!, descriptionTextView.text, reminderSwitch.isOn, dateAsString, date)
        } else {
            presenter.updateTask(userTask.id, titleTextField.text!, descriptionTextView.text, reminderSwitch.isOn, dateAsString, date)
        }
        
        
        
        closeView()
        
    }
    
    /*!
     * Called when user wants to leave this screen
     */
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Custom methods
    
    
    
    /*!
     * Called when user wants to leave this screen
     */
    func displayInputValidationError(_ message: String) {
        
        let alertController = UIAlertController(title: "Warning", message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    /*!
     * Called when user wants to leave this screen
     */
    internal func closeView() {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

}
