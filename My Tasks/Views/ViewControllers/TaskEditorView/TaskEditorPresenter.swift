//
//  AddNewTaskPresenter.swift
//  My Tasks
//
//  Created by User on 11/30/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation

protocol TaskEditorView {
    func displayInputValidationError(_ message: String)
    func closeView()
}

class TaskEditorPresenter{
    var taskEditorView: TaskEditorView?
    
    fileprivate var dbHelper: DBHelper!
    
    init() {
        dbHelper = DBHelper()
    }
    
    /*!
     * This method is used to assign a view to this presenter
     */
    func attachView(_ view: TaskEditorView) {
        self.taskEditorView = view
    }
    
    /*!
     * This method is used to unassign a view from this presenter
     */
    func detachView() {
        self.taskEditorView = nil
    }
    
    /*!
     * This method is used to create a new task and add it to the database
     */
    func addNewTask(_ title: String, _ description: String, _ hasNotification: Bool, _ dueDateAsString: String, _ dueDate: Date) {
        
        if title.isEmpty {
            taskEditorView?.displayInputValidationError("Title of the task cannot be empty!")
        } else {
            
            let currentDateAndTime = Utilities.getCurrentDateAndTimeAsString()
            
            if hasNotification {
                MyNotifications.setNotification(currentDateAndTime, title, description, dueDate)
            }
            
            dbHelper.createUserTask(currentDateAndTime, title, description, hasNotification, dueDate)
            
        }
        
    }
    
    /*!
     * This method is used to update a task in the the database
     */
    func updateTask(_ ID: String, _ title: String, _ description: String, _ hasNotification: Bool, _ dueDateAsString: String, _ dueDate: Date) {
        
        if title.isEmpty {
            taskEditorView?.displayInputValidationError("Title of the task cannot be empty!")
        } else {
            
            let userTask = UserTask()
            userTask.id = ID
            userTask.title = title
            userTask.description = description
            userTask.hasNotification = hasNotification
            userTask.notificationDate = dueDate
            
            if hasNotification {
                MyNotifications.setNotification(ID, title, description, dueDate)
            } else {
                MyNotifications.declineNotification(ID)
            }
            
            dbHelper.updateUserTask(userTask)
            
        }
        
    }
    
    
    /*!
     * This method returns a task from the database by the specified ID
     */
    func getTaskByID(_ taskID: String) -> UserTask  {
        let task = dbHelper.getUserTaskByID(taskID)
        
        if task.id.isEmpty && taskEditorView != nil {
            taskEditorView?.displayInputValidationError("No tasks found!")
            return task
        }
        
        return task
    }
    
}
