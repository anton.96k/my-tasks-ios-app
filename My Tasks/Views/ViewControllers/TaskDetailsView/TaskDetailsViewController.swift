//
//  TaskDetailsViewController.swift
//  My Tasks
//
//  Created by User on 12/5/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import UIKit

class TaskDetailsViewController: UIViewController, UIGestureRecognizerDelegate, TaskDetailsView {
    
    // MARK: - UI outlets and local variables
    
    // UI Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var hasReminderLabel: UILabel!
    @IBOutlet weak var reminderDateLabel: UILabel!
    @IBOutlet weak var reminderLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var reminderInfoView: UIView!
    
    @IBOutlet weak var editBarButton: UIBarButtonItem!
    @IBOutlet weak var changeTaskStatusButton: UIView!
    
    @IBOutlet weak var changeTaskStatusLabel: UILabel!
    @IBOutlet var changeTaskStatusTapRecognizer: UITapGestureRecognizer!
    
    // Local variables
    var userTaskID: String!
    var userTask: UserTask!
    var presenter: TaskDetailsPresenter!
    
    // MARK: - ViewController lifecycle methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Attaching the presenter
        presenter = TaskDetailsPresenter()
        presenter.attachView(self)
        
        // Setting up views
        descriptionLabel.sizeToFit()
        
        let changeStatusButtonTapGesture = UITapGestureRecognizer(target: self, action: #selector(TaskDetailsViewController.changeTaskStatus))
        changeStatusButtonTapGesture.delegate = self
        
        changeTaskStatusLabel.addGestureRecognizer(changeStatusButtonTapGesture)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Displaying the whole info about the task
        displayTaskDetails()
        
        // Changing the button accroding to the status of the task
        changeTaskStatusButton(userTask.isDone)
    }
    
    // MARK: - UI methods and actions
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Telling the destination view controller that we want to edit the task
        if segue.identifier == StoryboardIdentifiers.EDIT_TASK_SEGUE {
            let destinationViewController = segue.destination as? TaskEditorViewController
            destinationViewController?.isNewTask = false
            destinationViewController?.userTaskID = userTaskID
        }
    }
    
    // MARK: - Custom methods

    /*!
     * This method is used to display an alert dialog with a specified message
     */
    func displayAlert(_ message: String) {
        
        // Settin up the alert dialog
        let alertController = UIAlertController(title: "Warning", message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,handler: nil))
        
        // Presenting the dialog
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    /*!
     * This method is used to display an info about the task
     */
    func displayTaskDetails() {
        
        // Obtaning the task from the presenter
        userTask = presenter.getTaskByID(userTaskID)
        
        // Displaying the info
        titleLabel.text = userTask.title
        descriptionLabel.text = userTask.description
        
        if userTask.hasNotification {
            reminderLabel.text = "Has notification"
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.short
            
            dateLabel.text = dateFormatter.string(from: userTask.notificationDate)
        } else {
            reminderLabel.text = "Doesn't have a reminder"
            dateLabel.text = "No due date"
        }
        
    }
    
    /*!
     * This method is called when you need to change the status of the displayed task
     */
    @objc func changeTaskStatus() {
        
        // Changing the task status
        if userTask.isDone {
            userTask.isDone = false
            MyNotifications.setNotification(userTask.id, userTask.title, userTask.description, userTask.notificationDate)
        } else {
            userTask.isDone = true
            MyNotifications.declineNotification(userTask.id)
        }
        
        // Adjusting the button
        changeTaskStatusButton(userTask.isDone)
        
        // Telling the presenter to make changes in the model
        presenter.changeTaskStatus(userTask)
        
    }
    
    /*!
     * This method is called when you need to change the status button according to the new status of the task
     */
    fileprivate func changeTaskStatusButton(_ isDone: Bool) {
        
        if isDone {
            
            changeTaskStatusLabel.text = "Done"
            changeTaskStatusButton.backgroundColor = UIColor(displayP3Red: 94.0/255, green: 255.0/255, blue: 104.0/255, alpha: 1.0)
            
        } else {
            
            changeTaskStatusLabel.text = "Mark as done"
            changeTaskStatusButton.backgroundColor = UIColor(displayP3Red: 255.0/255, green: 208.0/255, blue: 51.0/255, alpha: 1.0)
            
        }
        
    }

}
