//
//  TaskDetailsPresenter.swift
//  My Tasks
//
//  Created by User on 12/12/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation

protocol TaskDetailsView {
    func displayAlert(_ message: String)
    func displayTaskDetails()
}

class TaskDetailsPresenter {
    
    var taskDetailsView: TaskDetailsView?
    var database: DBHelper!
    
    init() {
        database = DBHelper()
    }
    
    /*!
     * This method is used to assign a view to this presenter
     */
    func attachView(_ taskDetailsView: TaskDetailsView) {
        self.taskDetailsView = taskDetailsView
    }
    
    /*!
     * This method is used to unassign a view from this presenter
     */
    func detachView() {
        self.taskDetailsView = nil
    }
    
    /*!
     * This method returns a task from the database by the specified ID
     */
    func getTaskByID(_ taskID: String) -> UserTask  {
        let task = database.getUserTaskByID(taskID)
        
        if task.id.isEmpty && taskDetailsView != nil {
            taskDetailsView?.displayAlert("No tasks found!")
            return task
        }
        
        return task
    }
    
    /*!
     * This method changes the status of the task and saves changes to tthe database
     */
    func changeTaskStatus(_ userTask: UserTask) {        
        database.updateUserTask(userTask)
    }
    
}
