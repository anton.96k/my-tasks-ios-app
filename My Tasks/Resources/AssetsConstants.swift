//
//  AssetsConstants.swift
//  My Tasks
//
//  Created by User on 12/3/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation

class AssetsConstants {
    static let APP_ICON = "AppIcon"
    
    static let MY_TASKS_ICON = "MyTasks"
    static let AGENDA_ICON = "Agenda"
    static let BULB_ICON = "Bulb"
    static let GOAL_ICON = "Goal"
    static let WALK_ICON = "Walk"
    static let CALENDAR_ICON = "Calendar"
    static let ARCHIVE_ICON = "Archive"
    static let DESCRIPTION_ICON = "Description"
    static let TAG_ICON = "Tag"
    static let DOCUMENT_ICON = "Document"
    
    static let SPLASH_SCREEN_LOGO = "SplashScreenLogo"
    
}
