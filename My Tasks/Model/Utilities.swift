//
//  Utilities.swift
//  My Tasks
//
//  Created by User on 12/11/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation

class Utilities {
    
    /*!
     This method generates and returns a string that consists of current date and time
     */
    static func getCurrentDateAndTimeAsString() -> String {
        
        // String with date and time
        let dateTimeAsString: String!
        
        // Accessing the calendar and date
        let date = Date()
        let calendar = Calendar.current
        
        // Obtaining crrent date and time
        let year = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let hours = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        let nanoSeconds = calendar.component(.nanosecond, from: date)
        
        
        dateTimeAsString = "\(year)-\(month)-\(day) \(hours):\(minutes):\(seconds):\(nanoSeconds)"
        
        return dateTimeAsString
        
    }
}
