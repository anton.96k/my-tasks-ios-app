//
//  MyNotifications.swift
//  My Tasks
//
//  Created by User on 12/18/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation
import UserNotifications

class MyNotifications {
    
    /*!
     * This method sets up a notification that will be displayed at a defined date
     */
    static func setNotification(_ notificationID: String, _ subtitle: String, _ body: String, _ triggerDate: Date) {
        
        let content = UNMutableNotificationContent()
        content.title = "You have a task to do now"
        content.subtitle = subtitle
        content.body = body
        content.badge = 1
        
        let date = triggerDate
        let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        
        let notificationID = notificationID
        let notificationRequest = UNNotificationRequest(identifier: notificationID, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(
                                                notificationRequest,
                                                withCompletionHandler: {
                                                    (error) in
                                                }
        )
        
    }
    
    /*!
     * This method cancells a notification by it's identifier
     */
    static func declineNotification(_ notificationID: String) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [notificationID])
    }
    
}
