//
//  Task.swift
//  My Tasks
//
//  Created by User on 11/30/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation

class UserTask {
    var id: String
    var title: String
    var description: String
    var isDone: Bool
    var hasNotification: Bool
    var notificationDate: Date
    
    init() {
        id = ""
        title = ""
        description = ""
        isDone = false
        hasNotification = false
        notificationDate = Date()
    }
    
    init(_ title: String, _ description: String) {
        id = ""
        self.title = title
        self.description = description
        isDone = false
        hasNotification = false
        notificationDate = Date()
    }
    
    
}
