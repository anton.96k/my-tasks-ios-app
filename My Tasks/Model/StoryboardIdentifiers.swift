//
//  StoryboardIdentifiers.swift
//  My Tasks
//
//  Created by User on 12/6/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation

class StoryboardIdentifiers {
    static let TASKS_TABLEVIEW_CELL = "TasksTableViewCell"
    static let SHOW_TASK_DETAILS_SEGUE = "ShowTaskDetailsSegue"
    static let CREATE_NEW_TASK_SEGUE = "CreateNewTaskSegue"
    static let EDIT_TASK_SEGUE = "EditTaskSegue"
}
