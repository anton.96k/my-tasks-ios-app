//
//  TaskStatus.swift
//  My Tasks
//
//  Created by User on 11/30/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation

enum TaskStatus {
    case IN_PROGRESS
    case DONE
    case ALL
}
