//
//  File.swift
//  My Tasks
//
//  Created by User on 12/3/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation

class DBConstants {
    
    static let TASKS_ENTITY = "Task"
    
    static let TASK_ID = "taskID"
    static let TASK_TITLE = "taskTitle"
    static let TASK_DESCRIPTION = "taskDescription"
    static let TASK_IS_DONE = "taskIsDone"
    static let TASK_HAS_NOTIFICATION = "taskHasNotification"
    static let TASK_DUE_DATE = "taskDueDate"

    
    
}
