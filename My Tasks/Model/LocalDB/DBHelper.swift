//
//  DBHelper.swift
//  My Tasks
//
//  Created by User on 11/30/18.
//  Copyright © 2018 yobacorp. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DBHelper {
    
    // Local varibles
    fileprivate let appDelegate: AppDelegate
    fileprivate let context: NSManagedObjectContext
    
    /*!
     Default constructor
     */
    init() {
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
    }
    
    /*!
     This method creates a new entry in the databse to store the info about the task
     */
    func createUserTask(_ id: String, _ title: String, _ description: String, _ hasNotification: Bool, _ date: Date){
        
        let newUserTaskEntity = NSEntityDescription.entity(forEntityName: DBConstants.TASKS_ENTITY, in: context)
        let newUserTaskObject = NSManagedObject(entity: newUserTaskEntity!, insertInto: context)
        
        newUserTaskObject.setValue(id, forKey: DBConstants.TASK_ID)
        newUserTaskObject.setValue(title, forKey: DBConstants.TASK_TITLE)
        newUserTaskObject.setValue(description, forKey: DBConstants.TASK_DESCRIPTION)
        newUserTaskObject.setValue(false, forKey: DBConstants.TASK_IS_DONE)
        newUserTaskObject.setValue(hasNotification, forKey: DBConstants.TASK_HAS_NOTIFICATION)
        newUserTaskObject.setValue(date, forKey: DBConstants.TASK_DUE_DATE)
        
        do {
            try context.save()
        }
        catch {
            print("Save to DB failed")
        }
        
    }
    
    /*!
     This method is used to update the task by it's unique ID
     */
    func updateUserTask(_ userTask: UserTask) {
        
        // Setting up the fetch request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: DBConstants.TASKS_ENTITY)
        fetchRequest.predicate = NSPredicate(format: "\(DBConstants.TASK_ID) = %@", "\(userTask.id)")
        
        // Fetching the results
        if let result = try? context.fetch(fetchRequest) as! [NSManagedObject] {
            if result.count != 0 {
                
                let managedObject = result[0]
                managedObject.setValue(userTask.id, forKey: DBConstants.TASK_ID)
                managedObject.setValue(userTask.title, forKey: DBConstants.TASK_TITLE)
                managedObject.setValue(userTask.description, forKey: DBConstants.TASK_DESCRIPTION)
                managedObject.setValue(userTask.isDone, forKey: DBConstants.TASK_IS_DONE)
                managedObject.setValue(userTask.hasNotification, forKey: DBConstants.TASK_HAS_NOTIFICATION)
                managedObject.setValue(userTask.notificationDate, forKey: DBConstants.TASK_DUE_DATE)
                
            }
            do {
                try context.save()
            } catch {
                print ("Error while deleting the entity")
            }
        }
    }
    
    /*!
     This method is used to delete the task from the database by it's unique ID
     */
    func deleteUserTask(_ userTaskID: String) {
        
        // Setting up the fetch request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: DBConstants.TASKS_ENTITY)
        fetchRequest.predicate = NSPredicate(format: "\(DBConstants.TASK_ID) = %@", "\(userTaskID)")
        
        // Fetching the results
        if let result = try? context.fetch(fetchRequest) as! [NSManagedObject] {
            for entity in result {
                context.delete(entity)
            }
            do {
                try context.save()
            } catch {
                print ("Error while deleting the entity")
            }
        }
    }
    
    /*!
     This method returns a list of tasks from the database
     */
    func getUserTasks(_ category: TaskStatus) -> [UserTask] {
        
        var tasksList: [UserTask] = []
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: DBConstants.TASKS_ENTITY)
                switch category {
                case .ALL:
                    break
                case .DONE:
                    request.predicate = NSPredicate(format: "\(DBConstants.TASK_IS_DONE) = true")
                    break
                case .IN_PROGRESS:
                    request.predicate = NSPredicate(format: "\(DBConstants.TASK_IS_DONE) = false")
                    break
                }
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            
            for entry in result as! [NSManagedObject] {
                
                let userTask = UserTask()
                userTask.id = entry.value(forKey: DBConstants.TASK_ID) as! String
                userTask.title = entry.value(forKey: DBConstants.TASK_TITLE) as! String
                userTask.description = entry.value(forKey: DBConstants.TASK_DESCRIPTION) as! String
                userTask.isDone = entry.value(forKey: DBConstants.TASK_IS_DONE) as! Bool
                userTask.hasNotification = entry.value(forKey: DBConstants.TASK_HAS_NOTIFICATION) as! Bool
                userTask.notificationDate = entry.value(forKey: DBConstants.TASK_DUE_DATE) as! Date
                
                tasksList.append(userTask)
                
            }
        } catch {
            print("Error fetching the results")
        }
        
        return tasksList
        
    }
    
    /*!
     This method returns a task by it's ID
     */
    func getUserTaskByID(_ taskID: String) -> UserTask {
        
        let userTask = UserTask()
        
        // Setting up the fetch request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: DBConstants.TASKS_ENTITY)
        fetchRequest.predicate = NSPredicate(format: "\(DBConstants.TASK_ID) = %@", "\(taskID)")
        fetchRequest.returnsObjectsAsFaults = false
        
        // Fetching the results
        do {
            let result = try context.fetch(fetchRequest) as![NSManagedObject]
            
            if result.count == 1 {
                
                userTask.id = result[0].value(forKey: DBConstants.TASK_ID) as! String
                userTask.title = result[0].value(forKey: DBConstants.TASK_TITLE) as! String
                userTask.description = result[0].value(forKey: DBConstants.TASK_DESCRIPTION) as! String
                userTask.isDone = result[0].value(forKey: DBConstants.TASK_IS_DONE) as! Bool
                userTask.hasNotification = result[0].value(forKey: DBConstants.TASK_HAS_NOTIFICATION) as! Bool
                userTask.notificationDate = result[0].value(forKey: DBConstants.TASK_DUE_DATE) as! Date
            }
        } catch {
            print("Error fetching the results")
        }
        
        return userTask
    }
    
}
